import React from "react";
import { Box, Link, Flex, Button } from "@chakra-ui/react";
import NextLink from "next/link";
import { useMeQuery } from "../generated/graphql";

interface NavBarProps {}

export const NavBar: React.FC<NavBarProps> = ({}) => {
	const [{ data, fetching }] = useMeQuery();
	let body = null;

	if (fetching) {
		body = null;
	} else if (!data?.me) {
		body = (
			<>
				<Box ml={"auto"}>
					<NextLink href="/login">
						<Link mr={2}>Login</Link>
					</NextLink>
				</Box>
				<Box>
					<NextLink href="/register">
						<Link>Register</Link>
					</NextLink>
				</Box>
			</>
		);
	} else {
		body = (
			<>
				<Flex>
					<Box>Hi {data.me.username}</Box>
					<Box>
						<Button>Dashboard</Button>
						<Button>Logout</Button>
					</Box>
				</Flex>
			</>
		);
	}

	return (
		<Flex bg="teal" p={4}>
			{body}
		</Flex>
	);
};
