import { Box } from '@chakra-ui/layout'
import React from 'react'


interface wrapperProps{

}

export const Wrapper: React.FC<wrapperProps> = ({children}) => {
    return(
    <Box mt={8} mx='auto' maxW='800px' w='100%'>
        {children}
    </Box>
    )
}