import React from "react";
import { Formik, Form } from "formik";
import { Button, Box, useToast } from "@chakra-ui/react";
import { Wrapper } from "../components/Wrapper";
import { InputField } from "../components/InputField";
import { useLoginMutation } from "../generated/graphql";
import { toErrMap } from "../utils/toErrMap";
import { useRouter } from "next/router";
import { NavBar } from "../components/NavBar";

interface registerProps {}

const Login: React.FC<registerProps> = ({}) => {
	const [, login] = useLoginMutation();
	const router = useRouter();
	const toast = useToast();
	return (
		<>
			<NavBar />
			<Wrapper>
				<Formik
					initialValues={{ username: "", password: "" }}
					onSubmit={async (values, { setErrors }) => {
						const response = await login(values);
						if (response.data?.login?.errors) {
							setErrors(toErrMap(response.data.login.errors));
						} else if (response.data?.login?.user) {
							router.push("/");
							toast({
								title: response.data.login.user.username + " Logged In!",
								description: "Welcome Back!",
								status: "success",
								duration: 9000,
								isClosable: true,
							});
						}
					}}
				>
					{({ isSubmitting }) => (
						<Form>
							<Box mt={4}>
								<InputField
									name="username"
									label="Username"
									placeholder="Username"
								/>
							</Box>
							<Box mt={4}>
								<InputField
									name="password"
									label="Password"
									placeholder="Password"
									type="password"
								/>
							</Box>
							<Button
								mt={5}
								type="submit"
								isLoading={isSubmitting}
								colorScheme="teal"
							>
								Login
							</Button>
						</Form>
					)}
				</Formik>
			</Wrapper>
		</>
	);
};

export default Login;
