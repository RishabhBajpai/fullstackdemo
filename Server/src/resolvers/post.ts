
import { Post } from "../entities/post";
import { MyContext } from "src/types";
import { Arg, Ctx, Int, Mutation, Query, Resolver } from "type-graphql";

@Resolver()
export class PostResolver{
    @Query(() => [Post])
    posts(
        @Ctx() {em}:MyContext
    ): Promise<Post[]>{
        return em.find(Post,{});
    }
    @Query(() => Post, {nullable: true})
    post(
        @Arg('id', () => Int) _id : number,
        @Ctx() {em}:MyContext
    ): Promise<Post | null>{
        return em.findOne(Post,{_id});
    }
    @Mutation(() => Post)
    async createPost(
        @Arg('name', () => String) name : String,
        @Ctx() {em}:MyContext
    ): Promise<Post>{
        const post = em.create(Post, {name})
        em.persistAndFlush(post)
        return post
    }
    @Mutation(() => Post, {nullable: true})
    async updatePost(
        @Arg('id', ()=> Int) _id: number,
        @Arg('name', () => String, {nullable: true}) name : String,
        @Ctx() {em}:MyContext
    ): Promise<Post | null>{
        
        const post = await em.findOne(Post,{_id})
        
        if(!post){
            return null
        }
        
        if(typeof name !== 'undefined'){
            post.name = name
            await em.persistAndFlush(post)
        }
       
        return post
    }
    @Mutation(() => Boolean)
    async deletePost(
        @Arg('id', ()=> Int) _id: number,
        @Ctx() {em}:MyContext
    ): Promise<boolean>{
        try{
        await em.nativeDelete(Post,{_id})
        } catch{
            return false
        }
        return true;
    }
}