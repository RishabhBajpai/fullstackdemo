import { User } from "../entities/user";
import { MyContext } from "src/types";
import {
	Arg,
	Ctx,
	Field,
	InputType,
	Int,
	Mutation,
	ObjectType,
	Query,
	Resolver,
} from "type-graphql";
import argon2 from "argon2";

@InputType()
class UserLoginInput {
	@Field()
	username: string;

	@Field()
	password: string;
}

@ObjectType()
class FieldError {
	@Field()
	field: string;

	@Field()
	message: string;
}

@ObjectType()
class UserResponse {
	@Field(() => [FieldError], { nullable: true })
	errors?: FieldError[];

	@Field(() => User, { nullable: true })
	user?: User;
}

@Resolver()
export class UserResolver {
	@Query(() => User, { nullable: true })
	async me(@Ctx() { req, em }: MyContext) {
		if (!req.session.userId) {
			return null;
		}

		const user = await em.findOne(User, { _id: req.session.userId });
		return user;
	}

	@Query(() => [User])
	users(@Ctx() { em }: MyContext): Promise<User[]> {
		return em.find(User, {});
	}

	@Query(() => User, { nullable: true })
	user(
		@Arg("id", () => Int) _id: number,
		@Ctx() { em }: MyContext
	): Promise<User | null> {
		return em.findOne(User, { _id });
	}

	@Mutation(() => UserResponse)
	async register(
		@Arg("options") options: UserLoginInput,
		@Ctx() { em, req }: MyContext
	): Promise<UserResponse> {
		if (options.username.length <= 2) {
			return {
				errors: [
					{
						field: "username",
						message:
							"Username too short, please enter a username with at least 3 characters",
					},
				],
			};
		}
		if (!options.password.match("")) {
		}
		const dupeCheck = await em.count(User, { username: options.username });
		console.log(dupeCheck);
		if (dupeCheck < 1) {
			const hashedPwd = await argon2.hash(options.password);
			const user = em.create(User, {
				username: options.username,
				password: hashedPwd,
			});
			user.lastLogin = new Date();
			await em.persistAndFlush(user);
			req.session.userId = user._id;
			return { user: user };
		} else {
			return {
				errors: [
					{
						field: "username",
						message: "username already exists",
					},
				],
			};
		}
	}

	@Mutation(() => UserResponse, { nullable: true })
	async login(
		@Arg("options") options: UserLoginInput,
		@Ctx() { em, req }: MyContext
	): Promise<UserResponse> {
		const user = await em.findOne(User, { username: options.username });
		if (!user) {
			return {
				errors: [
					{
						field: "username",
						message: "Could not find specified user",
					},
				],
			};
		}
		const valid = await argon2.verify(user.password, options.password);
		if (!valid) {
			return {
				errors: [
					{
						field: "password",
						message: "Password was incorrect",
					},
				],
			};
		}
		user.lastLogin = new Date();
		em.persistAndFlush(user);

		req.session.userId = user._id;
		console.log(req.session.userId);

		return { user: user };
	}

	@Mutation(() => User, { nullable: true })
	async updateUsername(
		@Arg("id", () => Int) _id: number,
		@Arg("options") options: UserLoginInput,
		@Ctx() { em }: MyContext
	): Promise<User | null> {
		const user = await em.findOne(User, { _id });
		let changed = false;
		if (!user) {
			return null;
		}

		if (typeof options.username !== "undefined") {
			user.username = options.username;
			changed = true;
		}
		if (typeof options.password !== "undefined") {
			user.password = await argon2.hash(options.password);
			changed = true;
		}
		if (changed) {
			em.persistAndFlush(user);
			return user;
		} else {
			return null;
		}

		return user;
	}
	@Mutation(() => Boolean)
	async deleteUser(
		@Arg("id", () => Int) _id: number,
		@Ctx() { em }: MyContext
	): Promise<boolean> {
		try {
			await em.nativeDelete(User, { _id });
		} catch {
			return false;
		}
		return true;
	}
}
