import { MikroORM } from "@mikro-orm/core"
import { __prod__ } from "./constants"
import { Post } from "./entities/post"
import { User } from "./entities/user"
import path from 'path'

export default{
    migrations:{
        path: path.join(__dirname,'./migrations'), // path to the folder with migrations
        pattern: /^[\w-]+\d+\.[tj]s$/, // regex pattern for the migration files
    },
    entities: [Post,User],
    dbName: "appdb",
    debug: !__prod__,
    type: 'postgresql',
    host: 'ls-52bb9af8bb2041930d07011543e63f2377bcb75d.cuswqavomirt.us-west-2.rds.amazonaws.com',
    port: 5432,
    user: 'dbmasteruser',
    password: "-5hxgK2Fb(>TN8s~=!ulMho4<:Nk3-9w"
} as Parameters<typeof MikroORM.init>[0];
